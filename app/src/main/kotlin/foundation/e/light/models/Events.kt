package foundation.e.light.models

class Events {
    class StateChanged(val isEnabled: Boolean)

    class CameraUnavailable
}
